<?php

namespace App\Modules\Auth\src;

use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider {
    public function register() {
    }

    public function boot() {
        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'auth');
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');

        $this->publishes([
            __DIR__.'/public' => public_path('vendor/auth'),
        ], 'public');
    }
}



