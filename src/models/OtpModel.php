<?php

namespace App\Modules\Auth\src\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class OtpModel extends Authenticatable
{
	 //Use Updater;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'email', 'mobile_no', 'otp_send', 'otp', 'created_at', 'updated_at', 'status'];
    public $table       = 'login_with_otp';
}
