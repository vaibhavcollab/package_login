		<div class="container-fluid background_orange">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-3 my-3 my-md-2">
					<div class="row">
						<div class="col-md-4 text-center">
							<img src="<?php echo asset('/') . 'public-ui/images/';  ?>/phone.png">
						</div>
						<div class="col-md-8 text-center text-md-left px-0">
                            <span class="social-media font_16px"><b>{{ __('app.OFFICE_ADDRESS') }} : </b></span>
                            <span class="social-media">Aarambh, Office No 8, Datta Mandir  Rd,Wakad, Pune, Maharashtra 411057</span>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-3 my-3 my-md-2">
					<div class="row">
						<div class="col-md-4 text-center">
							<img src="<?php echo asset('/') . 'public-ui/images/';  ?>/phone.png">
						</div>
						<div class="col-md-8 text-center text-md-left px-0">
                            <span class="social-media font_16px"><b>{{ __('app.OFFICE_PHONE_NO') }} : </b></span>
                            <span class="social-media"><a href="tel:{{ __('app.PHONE_NO') }}" class="remove-text-decoration">{{ __('app.PHONE_NO') }}</a></span>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-3 my-3 my-md-2">
					<div class="row">
						<div class="col-md-4 text-center">
							<img src="<?php echo asset('/') . 'public-ui/images/';  ?>/mail.png">
						</div>
						<div class="col-md-8 text-center text-md-left px-0">
                            <span class="social-media font_16px"><b>{{ __('app.OFFICE_EMAIL') }} :</b></span>
                            <span class="social-media"><a href="mailto:sales@sharedmachine.in" class="remove-text-decoration">sales@sharedmachine.in</a></span>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-3 my-3 my-md-2">
					<div class="row">
						<div class="col-md-4 text-center">
							<img src="<?php echo asset('/') . 'public-ui/images/';  ?>/social-link.png">
						</div>
						<div class="col-md-8 text-center text-md-left px-0">
							<div class="social">
								<img src="<?php echo asset('/') . 'public-ui/images/';  ?>/Facebook_icon.png">
								<img src="<?php echo asset('/') . 'public-ui/images/';  ?>/Linkedin-icon.png">
								<img src="<?php echo asset('/') . 'public-ui/images/';  ?>/Google-Plus.png">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid" style="margin-bottom: 60px;">
			<div class="main-footer row">
				<div class="col-12 col-sm-6 col-md main-footer-content justify-content-center">
					<div class="footer-list-item"> <b>{{ __('app.FOOTER_SEC1') }}</b> </div>
					<div class="footer-list-item"> <img src="<?php echo asset('/') . 'public-ui/images/';  ?>/icon1.png"> </div>
					<div class="footer-list-item"> <a class="text_black" href="<?php echo URL::to('/about-us'); ?>">{{ __('app.FOOTER_SEC1_TEXT1') }} </a></div>
					<div class="footer-list-item"> <a class="text_black" href="<?php echo URL::to('/how-its-work'); ?>">{{ __('app.FOOTER_SEC1_TEXT2') }} </a></div>
					<div class="footer-list-item"> <a class="text_black" href="#">{{ __('app.FOOTER_SEC1_TEXT3') }} </a></div>
					<div class="footer-list-item"> <a class="text_black" href="#">{{ __('app.FOOTER_SEC1_TEXT4') }} </a></div>
				</div>

				<div class="col-12 col-sm-6 col-md main-footer-content justify-content-center">
					<div class="footer-list-item"> <b>{{ __('app.FOOTER_SEC2') }}</b> </div>
					<div class="footer-list-item"> <a href="#"><img src="<?php echo asset('/') . 'public-ui/images/';  ?>/icon1.png"> </a></div>
					<div class="footer-list-item"> <a class="text_black" href="#">{{ __('app.FOOTER_SEC2_TEXT1') }} </a></div>
					<div class="footer-list-item"> <a class="text_black" href="<?php echo URL::to('/privacy-policy'); ?>">{{ __('app.FOOTER_SEC2_TEXT2') }} </a></div>
					<div class="footer-list-item"> <a class="text_black" href="#">{{ __('app.FOOTER_SEC2_TEXT3') }} </a></div>
					<div class="footer-list-item"> <a class="text_black" href="#"> {{ __('app.FOOTER_SEC2_TEXT4') }}</a></div>
				</div>

				<div class="col-12 col-sm-6 col-md main-footer-content justify-content-center">
					<div class="footer-list-item"> <b>{{ __('app.FOOTER_SEC3') }}</b> </div>
					<div class="footer-list-item"> <a href="#"><img src="<?php echo asset('/') . 'public-ui/images/';  ?>/icon1.png"> </a></div>
					<div class="footer-list-item"> <a class="text_black" href="faq">{{ __('app.FOOTER_SEC3_TEXT1') }}</a></div>
					<div class="footer-list-item"> <a class="text_black" href="contact-us">{{ __('app.FOOTER_SEC3_TEXT2') }}</a></div>
					<div class="footer-list-item"> <a class="text_black" href="#">{{ __('app.FOOTER_SEC3_TEXT3') }} </a></div>
					<div class="footer-list-item"> <a class="text_black" href="#">{{ __('app.FOOTER_SEC3_TEXT4') }}</a></div>
				</div>

				<div class="col-12 col-sm-6 col-md main-footer-content justify-content-center">
					<div class="footer-list-item"> <b>{{ __('app.FOOTER_SEC4') }}</b> </div>
					<div class="footer-list-item"> <a href="#"><img src="<?php echo asset('/') . 'public-ui/images/';  ?>/icon1.png"> </a></div>
					<div class="footer-list-item"> <a class="text_black" href="#">{{ __('app.FOOTER_SEC4_TEXT1') }}</a></div>
					<div class="footer-list-item"> <a class="text_black" href="#">{{ __('app.FOOTER_SEC4_TEXT2') }}</a></div>
					<div class="footer-list-item"> <a class="text_black" href="#">{{ __('app.FOOTER_SEC4_TEXT3') }} </a></div>
					<div class="footer-list-item"> <a class="text_black" href="#">{{ __('app.FOOTER_SEC4_TEXT4') }}</a></div>
				</div>

				<div class="col-12 col-sm-6 col-md main-footer-content justify-content-center">
					<div class="footer-list-item"> <b>{{ __('app.FOOTER_SEC5') }}</b> </div>
                    <div class="footer-list-item"> <img src="<?php echo asset('/') . 'public-ui/images/';  ?>icon1.png"> </div>
                    <div class="footer-list-item"> <img src="<?php echo asset('/') . 'public-ui/images/';  ?>barcode.png" style="height: 80px;margin-left: -8px;"></div>
					<div class="footer-list-item" style="padding-top: 50px;"> <a class="text_black" href="https://play.google.com/store/apps/details?id=com.sharedmachine.mobile" target="_blank" class="cursor"><img src="<?php echo asset('/') . 'public-ui/images/';  ?>Google-Play.png"></a></div>
					<div class="footer-list-item" style="padding-top: 30px;"> <a class="text_black" href="itmss://itunes.apple.com/us/app/shared-machine/id1134646148" target="_blank" class="cursor"><img src="<?php echo asset('/') . 'public-ui/images/';  ?>App-store.png"></a></div>
					<div class="footer-list-item"> </div>

				</div>
			</div>
        </div>
        <div class="container text-center">
            <span class="font_14px text_black Copyright">© Copyright 2017 Sharedmachine. All Rights Reserved</span>
        </div>
    </body>
</html>

