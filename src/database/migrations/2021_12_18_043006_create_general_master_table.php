<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_master', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('gen_id', 20)->comment('Combination of master_code+code');
            $table->string('code', 20);
            $table->string('value', 50)->nullable();
            $table->string('description', 100);
            $table->integer('master_id')->nullable();
            $table->string('master_code', 50);
            $table->string('master_description', 100);
            $table->string('parent_id', 20)->nullable();
            $table->integer('sequence')->nullable();
            $table->string('locale_code', 150)->nullable();
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->tinyInteger('is_deleted')->default('0');
            $table->tinyInteger('is_completed')->default('0');
            $table->integer('locked')->nullable()->default('0')->comment('0 -> No, 1 -> Yes');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_master');
    }
}
